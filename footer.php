<!--CREATED BY: Hernan Ayarza on 25/05/17 for Group Project -->
<footer class="footer">
    <div class="content has-text-centered">
        <p style="color: white;">
          <strong style="color: #E5303F;">TRAVELiO</strong>: Coded with <a><i class="im im-heart" style="font-size: 0.8rem;"></a></i> by: <a>CGI</a>
        </p>
    </div>
</footer>
</body>
</html>
