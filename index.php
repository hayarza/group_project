<!--CREATED BY: Hernan Ayarza on 25/05/17 for Group Project -->
<!DOCTYPE html>
<html>
  <head>
    <title>TRAVELiO</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <link rel="stylesheet" href="https://cdn.iconmonstr.com/1.2.0/css/iconmonstr-iconic-font.min.css" />
    <script src="https://use.fontawesome.com/275c14323b.js"></script>
  </head>
<?php include_once("header.php"); ?>
<?php include_once("footer.php"); ?>
