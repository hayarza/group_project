<!--CREATED BY: Hernan Ayarza on 25/05/17 for Group Project -->

<body>
      <nav class="nav has-shadow">
          <div class="nav-left">
            <a class="nav-item" href="">
              TRAVELiO
              <i class="im im-paperplane" style="font-size: 1.8rem;"></i>
            </a>
          </div>

          <!-- This "nav-menu" is hidden on mobile -->
          <!-- Add the modifier "is-active" to display it on mobile -->
          <div class="nav-right nav-menu is-active">
            <a class="nav-item is-tab" href="">
              HOME
            </a>
            <a class="nav-item is-tab" href="">
              REGISTER
            </a>
            <a class="nav-item is-tab" href="">
              PACKAGES
            </a>
            <a class="nav-item is-tab" href="">
              CONTACT
            </a>
            <a class="nav-item is-tab" href="">
              LOG IN
            </a>
          </div>
      </nav>
